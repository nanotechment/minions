package com.example.minions.util;

public class Constant {
    public static final String DISH_API = "api/dish";
    public static final String API_DOMAIN = "http://localhost:8080";
    public static final String DISH_IMAGE =  "/Minions/dishImage/";
    public static final String ADMIN_API = "api/admin";
    public static final String ACCOUNT_API = "api/account";
    public static final String OTP_CODE_LINK = API_DOMAIN + "/" + ACCOUNT_API +"/verifyEmail?code=";

}
